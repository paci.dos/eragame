"""
EraGame
"""
import os
import sys
import random
import secrets
import re
import argparse

class Stone:
    """ Class Stone contains stone attributes and methods """
    def __init__(self, shape, color, symbol, symbol_color):
        self.shape = shape
        self.color = color
        self.symbol = symbol
        self.symbol_color = symbol_color

    def get_upper_pair(self):
        """ Return upper pair of stone characteristics """
        bin_string = self.shape + self.color

        return bin_string

    def get_lower_pair(self):
        """ Return lower pair of stone characteristics """
        bin_string = self.symbol + self.symbol_color

        return bin_string

    def get_stone_string(self):
        """ Return all of stone characteristics """
        bin_string = self.shape + self.color + self.symbol + self.symbol_color

        return bin_string

class Board:
    """ Class Board contains board attributes and methods """
    # pylint: disable=too-many-instance-attributes
    def __init__(self):
        self.field_11 = "-"
        self.field_12 = "-"
        self.field_13 = "-"
        self.field_14 = "-"
        self.field_21 = "-"
        self.field_22 = "-"
        self.field_23 = "-"
        self.field_24 = "-"
        self.field_31 = "-"
        self.field_32 = "-"
        self.field_33 = "-"
        self.field_34 = "-"
        self.field_41 = "-"
        self.field_42 = "-"
        self.field_43 = "-"
        self.field_44 = "-"

    def display_board(self):
        """ Return whole board """
        print("---------------------")
        for i in range(1, 5):
            upper_line = "|"
            lower_line = "|"
            for k in range(1, 5):
                field = getattr(self, 'field_' + str(i) + str(k))
                if field != "-":
                    upper_line = upper_line + (f" {field.get_upper_pair()} |")
                    lower_line = lower_line + (f" {field.get_lower_pair()} |")
                else:
                    upper_line = upper_line + (" -- |")
                    lower_line = lower_line + (" -- |")

            print(upper_line)
            print(lower_line)
            print("---------------------")

    def place_stone(self, coordinates, stone):
        """ Place stone on board """
        setattr(self, 'field_' + str(coordinates), stone)

    def is_field_avaible(self, coordinates):
        """ Return True if field on board is empty """
        field = getattr(self, 'field_' + coordinates)

        if field == "-":
            return True

        return False

    def status(self, player):
        """ Return a game status """
        status_dict = {
                    "LU" : [],
                    "LD" : [],
                    "RU" : [],
                    "RD" : []
                }

        columns_dict = {
                    "LU" : [],
                    "LD" : [],
                    "RU" : [],
                    "RD" : []
                }

        #check all rows & colums
        for i in range(1, 5):
            for k in range(1, 5):
                row_field = getattr(self, 'field_' + str(i) + str(k))
                column_field = getattr(self, 'field_' + str(k) + str(i))
                status_dict = self.add_field_to_dict(status_dict, row_field)
                columns_dict = self.add_field_to_dict(columns_dict, column_field)

            self.are_elements_equal(status_dict, player)
            self.are_elements_equal(columns_dict, player)
            status_dict = self.clear_status_dict(status_dict)
            columns_dict = self.clear_status_dict(columns_dict)

        #check main diagonal
        main_diagonal = ["11", "22", "33", "44"]
        for coordinate in main_diagonal:
            field = getattr(self, 'field_' + coordinate)
            status_dict = self.add_field_to_dict(status_dict, field)
        self.are_elements_equal(status_dict, player)
        status_dict = self.clear_status_dict(status_dict)

        #check secondary diagonal
        secondary_diagonal = ["14", "23", "32", "41"]
        for coordinate in secondary_diagonal:
            field = getattr(self, 'field_' + coordinate)
            status_dict = self.add_field_to_dict(status_dict, field)
        self.are_elements_equal(status_dict, player)
        status_dict = self.clear_status_dict(status_dict)

    @classmethod
    def clear_status_dict(cls, general_dict):
        """ Clear status dictionary """
        general_dict = {
                    "LU" : [],
                    "LD" : [],
                    "RU" : [],
                    "RD" : []
                }

        return general_dict

    @classmethod
    def add_field_to_dict(cls, general_dict, field):
        """ Add field to dictionary """
        if field != "-":
            general_dict['LU'].append(field.shape)
            general_dict['RU'].append(field.color)
            general_dict['LD'].append(field.symbol)
            general_dict['RD'].append(field.symbol_color)
        else:
            general_dict['LU'].append("-")
            general_dict['RU'].append("-")
            general_dict['LD'].append("-")
            general_dict['RD'].append("-")

        return general_dict

    @classmethod
    def are_elements_equal(cls, dict_of_elements, player):
        """ Check if player placed winning stone on board """
        for list_of_elements in dict_of_elements.values():
            zeroes = list_of_elements.count("0")
            ones = list_of_elements.count("1")
            if zeroes == 4 or ones == 4:
                print("***********************")
                print(f"**** {player} wins ****")
                print("***********************")
                sys.exit()


################################################################################################
################################################################################################
################################################################################################

def main():
    """ Main function of program """
    os.system('cls')
    parser = argparse.ArgumentParser()
    parser.add_argument("--player1", action='store_true', help = "Let computer start")
    parser.add_argument("--TwoPlayers", action='store_true', help = "For testing functionality")
    args = parser.parse_args()

    list_of_stones = generate_stones()
    board = Board()
    board.display_board()
    display_remaining_stones(list_of_stones, "It is first turn now.")

    if args.player1:
        while True:
            computer_move(board, list_of_stones)
            user_move(board, list_of_stones)
    elif args.TwoPlayers:
        while True:
            two_users_game(board, list_of_stones)
    else:
        while True:
            user_move(board, list_of_stones)
            computer_move(board, list_of_stones)


################################################################################################
################################################################################################
################################################################################################

def two_users_game(board, list_of_stones):
    """ Run game for two players """
    #player 1
    is_avaible_any_stone(list_of_stones)
    stone = user_choose_stone(list_of_stones)
    list_of_stones.remove(stone)
    coordinates = user_select_coordinates(board)
    place_stone_on_board(coordinates, board, stone, list_of_stones)
    board.status('Player 1')

    #player 2
    is_avaible_any_stone(list_of_stones)
    stone = user_choose_stone(list_of_stones)
    list_of_stones.remove(stone)
    coordinates = user_select_coordinates(board)
    place_stone_on_board(coordinates, board, stone, list_of_stones)
    board.status('Player 2')

################################################################################################
################################################################################################

def user_move(board, list_of_stones):
    """ Run user move """
    is_avaible_any_stone(list_of_stones)
    stone = user_choose_stone(list_of_stones)
    list_of_stones.remove(stone)

    coordinates = computer_choose_random_field(board)
    place_stone_on_board(coordinates, board, stone, list_of_stones)
    board.status('Computer')

def computer_move(board, list_of_stones):
    """ Run computer move """
    is_avaible_any_stone(list_of_stones)
    stone = computer_choose_random_stone(list_of_stones)
    list_of_stones.remove(stone)

    coordinates = user_select_coordinates(board)
    place_stone_on_board(coordinates, board, stone, list_of_stones)
    board.status('Player')

################################################################################################
################################################################################################

def user_select_coordinates(board):
    """ Return coordinates selected by User """
    invalid_coordinates = True
    while invalid_coordinates:

        coordinates = input("Placing stone on field: ")
        is_input_exit(coordinates)

        valid_input = validate_coordinates_input(coordinates)

        if valid_input is False:
            print("----\nInvalid input. Choose another one. \n")

        if valid_input:
            if board.is_field_avaible(coordinates):
                invalid_coordinates = False
            else:
                print("----\nThis field already contains a stone. Choose another one. \n")

    return coordinates

def user_choose_stone(avaible_stones):
    """ Return stone selected by User """
    invalid_stone = True
    while invalid_stone:
        stone_string = input("Choose stone: ")
        is_input_exit(stone_string)

        if (re.search(r"^[0-1]{4}", stone_string)) and (len(stone_string) == 4):
            for stone in avaible_stones:
                if stone.shape == stone_string[0] and stone.color == stone_string[1] and  \
                stone.symbol == stone_string[2] and stone.symbol_color == stone_string[3]:
                    print(f"Giving choosen stone: {stone.get_stone_string()}")
                    return stone

        print("Invalid stone. Choose another one.\n")

def validate_coordinates_input(coordinates):
    """ Return True if coordinates are valid """
    if (re.search(r"^[1-4]{2}", coordinates))  and (len(coordinates) == 2):
        return True

    return False

################################################################################################
################################################################################################

def computer_choose_random_stone(avaible_stones):
    """ Return stone selected by Computer """
    stone = secrets.choice(avaible_stones)
    print(f"Giving choosen stone: {stone.get_stone_string()}")

    return stone

def computer_choose_random_field(board):
    """ Return coordinates selected by Computer """
    invalid_coordinates = True
    while invalid_coordinates:
        row = random.randint(1, 4)
        col = random.randint(1, 4)

        coordinates = str(row) + str(col)

        if board.is_field_avaible(coordinates):
            invalid_coordinates = False

    return str(coordinates)

################################################################################################
################################################################################################

def generate_stones():
    """ Return list of generated stones """
    list_of_stones = []
    for i in range(16):
        temp = "{0:04b}".format(i)
        stone = Stone(temp[0],temp[1],temp[2],temp[3])
        list_of_stones.append(stone)

    random.shuffle(list_of_stones)

    return list_of_stones

def is_avaible_any_stone(list_of_stones):
    """ Exit the game if there is no avaible stone """
    list_length = len(list_of_stones)
    if list_length == 0:
        print("no winner")
        sys.exit()

def place_stone_on_board(coordinates, board, stone, list_of_stones):
    """ Place stone on board and display remaining stones """
    board.place_stone(coordinates, stone)
    os.system('cls')
    board.display_board()
    display_remaining_stones(list_of_stones, coordinates, stone.get_stone_string())

def display_remaining_stones(remain_stones_list, coordinates, placed_stone = ""):
    """ Display remaining stones and previously placed stone """
    upper_line = "| "
    lower_line = "| "
    for stone in remain_stones_list:
        upper_line = upper_line + stone.get_upper_pair() + " | "
        lower_line = lower_line + stone.get_lower_pair() + " | "

    print("Remaining stones:")
    print(upper_line)
    print(lower_line)
    print("--------------------------------------\n")
    print("You can end game by typing 'exit'\n")
    print("Game status: playing\n")
    print("--------------------------------------\n")
    print(f"Stone: {placed_stone} was placed on field: {coordinates}")

def is_input_exit(user_input):
    """ Check if input is exit """
    if user_input == "exit":
        sys.exit()

################################################################################################
################################################################################################
################################################################################################

if __name__ == "__main__":
    main()
